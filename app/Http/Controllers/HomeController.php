<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\empleado;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $empleados = empleado::all();
        $total_empleados = empleado::count();
        if( $total_empleados == 0){
            return view('sindatos');
        } else {
            return view('home', compact('empleados', 'total_empleados'));
        }
        
    }

    public function deshabilitar($id) {
        $empleado = empleado::find($id);
        $empleado->activo = 0;
        $empleado->save();
        return json_encode(array('estatus' => true, 'mensaje' => 'Empleado deshabilidado'));
    }

    public function habilitar($id) {
        $empleado = empleado::find($id);
        $empleado->activo = 1;
        $empleado->save();
        return json_encode(array('estatus' => true, 'mensaje' => 'Empleado deshabilidado'));
    }

    public function formulario() {
        return view('formulario');
    }

    public function crear(Request $request) {

        $resultado['estatus'] = true;
        $resultado['mensaje'] = array();
        $array_validaciones = [
            'codigo' => 'required',
            'nombre' => 'required',
            'salarioDolares' => '',
            'salarioPesos' => 'required',
            'direccion' => 'required',
            'estado' => 'required',
            'ciudad' => 'required',
            'telefono' => 'required',
            'correo' => 'required|email'
        ];
        
        $validacion  = Validator::make($request->all(), $array_validaciones);
        if ( $validacion->fails() ) {
            $resultado['estatus'] = false;
            $resultado['mensaje'] = $validacion->errors();
            return json_encode($resultado);
        }else {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/oportuno?token=b81b58622668895fee70a134e5bece33221b36650d1f3aa2bb8f0823170e7d76');
            $data = json_decode($response->getBody(), true);
            $fecha =  $data['bmx']['series'][0]['datos'][0]['fecha'];
            $dolar =  $data['bmx']['series'][0]['datos'][0]['dato'];
            $cont = request()->all();
            $cont['salarioDolares'] = $cont['salarioPesos'] / $dolar;

            if(empleado::where('codigo',$request->input('codigo'))->count() > 0) {
                $resultado['estatus'] = false;
                $resultado['mensaje'] =  $validacion->errors()->add('codigo', 'Codigo existe');
                return json_encode($resultado);
            }else {
                if( $cont['salarioPesos'] == 0 ){
                    $resultado['estatus'] = false;
                    $resultado['mensaje'] =  $validacion->errors()->add('salarioPesos', 'el salario debe ser mayor a 0');
                    return json_encode($resultado);
                }else {
                    if($this->quitar_tildes($cont['nombre'])){
                        $resultado['estatus'] = false;
                        $resultado['mensaje'] =  $validacion->errors()->add('nombre', 'caracteres no permitidos');
                        return json_encode($resultado);
                    }else{
                        empleado::create($cont);
                        return json_encode($resultado);
                    }
                }
            }
        }
    }

    function quitar_tildes($cadena) {
        if (strstr($cadena, 'ñ')) {
            return true;
        }
        if (strstr($cadena, 'Ñ')) {
            return true;
        }
        if (strstr($cadena, 'á')) {
            return true;
        }
        if (strstr($cadena, 'Á')) {
            return true;
        }
        if (strstr($cadena, 'é')) {
            return true;
        }
        if (strstr($cadena, 'É')) {
            return true;
        }
        if (strstr($cadena, 'o')) {
            return true;
        }
        if (strstr($cadena, 'ó')) {
            return true;
        }
        if (strstr($cadena, 'í')) {
            return true;
        }
        if (strstr($cadena, 'Í')) {
            return true;
        }
        if (strstr($cadena, 'Ú')) {
            return true;
        }
        if (strstr($cadena, 'ú')) {
            return true;
        }
    }

    public function eliminar($id)
    {
        $cont = empleado::find($id);
        $cont->delete();
        return redirect()->action([HomeController::class, 'index']);
    }

    public function editar($id) {
        $empleado = empleado::find($id);
        return view('formulario_editar', compact('empleado'));
    }

    public function guardarEditar(Request $request){
        $resultado['estatus'] = true;
        $resultado['mensaje'] = array();
        $array_validaciones = [
            'id' => 'required',
            'codigo' => 'required',
            'nombre' => 'required',
            'salarioDolares' => '',
            'salarioPesos' => 'required',
            'direccion' => 'required',
            'estado' => 'required',
            'ciudad' => 'required',
            'telefono' => 'required',
            'correo' => 'required|email'
        ];
        
        $validacion  = Validator::make($request->all(), $array_validaciones);
        if ( $validacion->fails() ) {
            $resultado['estatus'] = false;
            $resultado['mensaje'] = $validacion->errors();
            return json_encode($resultado);
        }else {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/oportuno?token=b81b58622668895fee70a134e5bece33221b36650d1f3aa2bb8f0823170e7d76');
            $data = json_decode($response->getBody(), true);
            $fecha =  $data['bmx']['series'][0]['datos'][0]['fecha'];
            $dolar =  $data['bmx']['series'][0]['datos'][0]['dato'];
            $cont = request()->all();
            $cont['salarioDolares'] = $cont['salarioPesos'] / $dolar;

            if(empleado::where('codigo',$request->input('codigo'))->where('id', '!=', $request->input('id'))->count() > 0) {
                $resultado['estatus'] = false;
                $resultado['mensaje'] =  $validacion->errors()->add('codigo', 'Codigo existe');
                return json_encode($resultado);
            }else {
                if( $cont['salarioPesos'] == 0 ){
                    $resultado['estatus'] = false;
                    $resultado['mensaje'] =  $validacion->errors()->add('salarioPesos', 'el salario debe ser mayor a 0');
                    return json_encode($resultado);
                }

                $Nempleado = empleado::find($cont['id']);
                $Nempleado->codigo = $cont['codigo'];
                $Nempleado->nombre = $cont['nombre'];
                $Nempleado->salarioDolares = $cont['salarioDolares'];
                $Nempleado->salarioPesos = $cont['salarioPesos'];
                $Nempleado->direccion = $cont['direccion'];
                $Nempleado->estado = $cont['estado'];
                $Nempleado->ciudad = $cont['ciudad'];
                $Nempleado->telefono = $cont['telefono'];
                $Nempleado->correo = $cont['correo'];
                if($this->quitar_tildes($cont['nombre'])){
                    $resultado['estatus'] = false;
                    $resultado['mensaje'] =  $validacion->errors()->add('nombre', 'caracteres no permitidos');
                    return json_encode($resultado);
                }else{
                    $Nempleado->save();
                    return json_encode(array('estatus' => true, 'mensaje' => 'Empresario editado'));
                }  
            }
        }
    }
}

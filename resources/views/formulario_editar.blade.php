@extends('layouts.app')

@section('content')
    <div class="content" style="padding: 20px;">
        <form id="formAgregar">
            @csrf
            <input name="id" type="number" hidden class="form-control" id="codigo"  placeholder="codigo..." value="{{$empleado->id}}">
            <div class="form-group">
                <label for="codigo">Código</label>
            <input name="codigo" type="text" class="form-control" id="codigo"  placeholder="codigo..." value="{{$empleado->codigo}}">
                <small id="codigoError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input name="nombre" type="text" class="form-control" id="nombre"  placeholder="nombre..." value="{{$empleado->nombre}}">
                <small id="nombreError" class="form-text text-muted"></small>
            </div>
            <div class="form-group" hidden>
                <label for="dolar">Salario en dolar</label>
                <input name="salarioDolares" type="number" class="form-control" id="dolar"  placeholder="dolar..." value="{{$empleado->salarioDolares}}">
                <small id="dolarError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="peso">Salario en pesos</label>
                <input name="salarioPesos" type="number" class="form-control" id="peso"  placeholder="peso..."value="{{$empleado->salarioPesos}}">
                <small id="pesosError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <input name="direccion" type="text" class="form-control" id="direccion"  placeholder="direccion..." value="{{$empleado->direccion}}">
                <small id="direccionError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="estado">Estado</label>
                <input name="estado" type="text" class="form-control" id="estado"  placeholder="estado..." value="{{$empleado->estado}}">
                <small id="estadoError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="ciudad">Ciudad</label>
                <input name="ciudad" type="text" class="form-control" id="ciudad"  placeholder="ciudad..." value="{{$empleado->ciudad}}">
                <small id="ciudadError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input name="telefono" type="text" class="form-control" id="telefono"  placeholder="telefono..."value="{{$empleado->telefono}}">
                <small id="telefonoError" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="correo">Correo</label>
                <input name="correo" type="email" class="form-control" id="correo"  placeholder="correo..." value="{{$empleado->correo}}">
                <small id="correoError" class="form-text text-muted"></small>
            </div>
            <div class="agregar" style="text-align: center">
                <button id="agregarBoton" type="button" class="btn btn-outline-primary btn-lg">Agregar</button>
                <button id="regresar" type="button" class="btn btn-outline-danger btn-lg" onclick="tabla()">Cancelar</button>
            </div>
        </form>
    </div>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script>

        $('.agregar').on('click','#agregarBoton',function (e){
            var dataForm = $('#formAgregar').serialize();
            $.ajax({
                headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                type : "POST",
                url : '{{route('home.guardarEditar')}}',
                data: dataForm,
                dataType : "json",
                success:function (response) {
                    console.log(response);
                    if ( response.estatus == false ) {
                        if ( response.mensaje.codigo != undefined ) {
                            $('#codigoError').html(response.mensaje.codigo);
                        }
                        if ( response.mensaje.nombre != undefined ) {
                            $('#nombreError').html(response.mensaje.nombre);
                        }
                        if ( response.mensaje.salarioDolares != undefined ) {
                            $('#dolarError').html(response.mensaje.dolar);
                        }
                        if ( response.mensaje.salarioPesos != undefined ) {
                            $('#pesosError').html(response.mensaje.salarioPesos);
                        }
                        if ( response.mensaje.direccion != undefined ) {
                            $('#direccionError').html(response.mensaje.direccion);
                        }
                        if ( response.mensaje.estado != undefined ) {
                            $('#estadoError').html(response.mensaje.estado);
                        }
                        if ( response.mensaje.ciudad != undefined ) {
                            $('#ciudadError').html(response.mensaje.ciudad);
                        }
                        if ( response.mensaje.telefono != undefined ) {
                            $('#telefonoError').html(response.mensaje.telefono);
                        }
                        if ( response.mensaje.correo != undefined ) {
                            $('#correoError').html(response.mensaje.correo);
                        }
                    }
                    if ( response.estatus == true ) {
                        window.location.replace("{{ route('home') }}");
                    }
                },
                error: function (xhr) {
                    alert(xhr.status);
                }
            });
        });    

         function tabla(){
            window.location.replace("{{ route('home') }}");
         }

    </script>
@endsection
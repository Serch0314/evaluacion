@extends('layouts.app')

@section('content')
<div class="container">
    <div style="margin-bottom: 20px;" class="agregar">
        <button id="boton" type="button" class="btn btn-outline-success btn-block" onclick="agregar()">
            Agregar empleado <i class="fa fa-plus-circle" aria-hidden="true"></i>
        </button>
    </div>
</div>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script>
    function agregar(){
        window.location.replace("{{ route('formulario') }}");
    }
</script>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div style="margin-bottom: 20px;" class="agregar">
            <button id="boton" type="button" class="btn btn-outline-success btn-block" onclick="agregar()">
                Agregar empleado <i class="fa fa-plus-circle" aria-hidden="true"></i>
            </button>
        </div>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Código</th>
                <th scope="col">Nombre</th>
                <th scope="col">Dolares</th>
                <th scope="col">Pesos</th>
                <th scope="col">Dirección</th>
                <th scope="col">Estado</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Telefono</th>
                <th scope="col">Correo</th>
                <th scope="col">Activo</th>
                <th scope="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
                @if($total_empleados > 0)

                    @foreach($empleados as $empleado)
                        <tr>
                            <td>{{ $empleado->codigo }}</td>
                            <td>{{ $empleado->nombre }}</td>
                            <td>{{ $empleado->salarioDolares }}</td>
                            <td>{{ $empleado->salarioPesos }}</td>
                            <td>{{ $empleado->direccion }}</td>
                            <td>{{ $empleado->estado }}</td>
                            <td>{{ $empleado->ciudad }}</td>
                            <td>{{ $empleado->telefono }}</td>
                            <td>{{ $empleado->correo }}</td>
                            <td class="activar">
                                @if( $empleado-> activo == 0)
                                    <button id="habilitar" type="button" class="btn btn-secondary">Desactivado</button>
                                @else
                                    <button id="desactivar" type="button" class="btn btn-success">Activado</button>
                                @endif
                            </td>
                            <td>
                                <div class="acciones" style="display: flex; justify-content: center; align-items: center;">
                                    <button type="button" class="btn btn-warning" onclick="editar({{$empleado->id}})">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>
                                    <button id="eliminar" type="button" class="btn btn-danger" onclick="eliminar({{ $empleado->id }})">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
              
            </tbody>
          </table>
    </div>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script>

         $('.activar').on('click','#habilitar',function (e){
            $.ajax({
                url : '{{route('habilitar',['id' => $empleado->id ])}}',
                dataType : "json",
                success: function(response) {
                    window.location.replace("{{ route('home') }}");
                },
                error: function(xhr) {
                    alert(xhr.status);
                }
            });
         });

         $('.activar').on('click','#desactivar',function (e){
            $.ajax({
                url : '{{route('deshabilitar',['id' => $empleado->id ])}}',
                dataType : "json",
                success: function(response) {
                    window.location.replace("{{ route('home') }}");
                },
                error: function(xhr) {
                    alert(xhr.status);
                }
            });
         });

         function agregar(){
            window.location.replace("{{ route('formulario') }}");
         }

         function eliminar(id) {
            var url = '{{ route("home.eliminar", ":id") }}';
            url = url.replace(':id', id);
            window.location.href = url;
        }

        function editar(empleado) {
            var url = '{{ route("home.editar", ":empleado") }}';
            url = url.replace(':empleado', empleado);
            window.location.href = url;
        }

    </script>
@endsection

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/deshabilitar/{id}', 'HomeController@deshabilitar')->name('deshabilitar');
Route::get('/home/habilitar/{id}', 'HomeController@habilitar')->name('habilitar');
Route::get('/home/formulario', 'HomeController@formulario')->name('formulario');
Route::post('/home/crear', 'HomeController@crear')->name('home.crear');
Route::get('/home/eliminar/{id}', 'HomeController@eliminar')->name('home.eliminar');
Route::get('/home/editar/{empleado}', 'HomeController@editar')->name('home.editar');
Route::post('/home/edicion', 'HomeController@guardarEditar')->name('home.guardarEditar');